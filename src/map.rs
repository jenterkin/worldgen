pub type Map3DDataCol = Vec<bool>;
pub type Map3DDataLayer = Vec<Map3DDataCol>;
pub type Map3DData = Vec<Map3DDataLayer>;

use crate::heightmap::HeightMap;

pub struct Map3DDimensions {
    pub height: u32,
    pub width: u32,
    pub depth: u32,
}

pub struct Map3D {
    pub dimensions: Map3DDimensions,
    pub seed: u32,
    pub data: Map3DData,
}

impl Map3D {
    /// Creates a new `Map3D` instance.
    /// # Examples
    /// ```
    /// use world_gen::map::Map3D;
    ///
    /// let height = 300;
    /// let width = 400;
    /// let depth = 255;
    /// let seed = 12345;
    /// let map = Map3D::new(height, width, depth, seed);
    /// ```
    pub fn new(height: u32, width: u32, depth: u32, seed: u32) -> Map3D {
        let dimensions = Map3DDimensions {
            height,
            width,
            depth,
        };
        let map_data = Map3D::gen(&dimensions, seed);
        Map3D {
            dimensions,
            seed,
            data: map_data,
        }
    }

    fn gen(dimensions: &Map3DDimensions, seed: u32) -> Map3DData {
        let height_map = HeightMap::new(dimensions.height, dimensions.width, seed);
        let mut map_data = Map3DData::new();
        for level in 0..dimensions.depth {
            map_data.push(Map3D::get_data_layer(dimensions.depth, level, &height_map));
        }
        map_data
    }

    fn get_data_layer(depth: u32, level: u32, height_map: &HeightMap) -> Map3DDataLayer {
        let mut data_layer = Map3DDataLayer::new();
        for x in 0usize..height_map.width as usize {
            let mut data_col = Map3DDataCol::new();
            for y in 0usize..height_map.height as usize {
                data_col.push(
                    if (height_map.data[x][y] * depth as f64).round() >= (depth - level) as f64 {
                        true
                    } else {
                        false
                    },
                );
            }
            data_layer.push(data_col);
        }
        data_layer
    }
}
