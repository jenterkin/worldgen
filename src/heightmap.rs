use noise::{Perlin, Seedable, NoiseFn};


pub type HeightMapDataCol  = Vec<f64>;
pub type HeightMapData = Vec<HeightMapDataCol>;

pub struct HeightMap {
    pub height: u32,
    pub width: u32,
    pub seed: u32,
    pub data: HeightMapData
}

impl HeightMap {
    /// Creates a new `HeightMap` instance.
    /// # Examples
    /// ```
    /// use world_gen::heightmap::HeightMap;
    ///
    /// let height = 300;
    /// let width = 400;
    /// let seed = 12345;
    /// let map = HeightMap::new(height, width, seed);
    /// for x in 0..height {
    ///     for y in 0..width {
    ///         // Iterates over each point in the heightmap
    ///     }
    /// }
    /// ```
    pub fn new(height: u32, width: u32, seed: u32) -> HeightMap {
        let map_data = HeightMap::gen(height, width, seed);
        HeightMap {
            height,
            width,
            seed,
            data: map_data
        }
    }

    fn gen(height: u32, width: u32, seed: u32) -> HeightMapData {
        let noise = Perlin::new().set_seed(seed);
        let mut map = HeightMapData::new();
        for x in 0..width {
            map.push(HeightMap::gen_column(x, height, width, noise));
        }
        map
    }

    fn gen_column(x: u32, height: u32, width: u32, noise: Perlin) -> HeightMapDataCol {
        let mut col = HeightMapDataCol::new();
        for y in 0..height {
            let nx: f64 = x as f64 / width as f64 - 0.5;
            let ny: f64 = y as f64 / height as f64 - 0.5;
            let elevation = HeightMap::get_point(nx, ny, noise);
            col.push(elevation);
        }
        col
    }

    fn get_point(x: f64, y: f64, noise: Perlin) -> f64 {
        let mut val = noise.get([x, y]);
        val += 0.5 * noise.get([2.0 * x, 2.0 * y]);
        val += 0.25 * noise.get([4.0 * x, 2.0 * y]);
        val += 1.0;
        val = val / 2.0;
        val
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_heightmap() {
        let height = 50;
        let width = 40;
        let seed = 12345;
        let map = HeightMap::new(height, width, seed);
        assert_eq!(map.height, height);
        assert_eq!(map.width, width);
        assert_eq!(map.seed, seed);
        assert_eq!(map.data.len() as u32, map.width);
        for x in 0..width {
            assert_eq!(map.data[x as usize].len() as u32, map.height);
        }
    }
}
